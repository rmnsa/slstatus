### Preview

![screenshot](https://i.postimg.cc/ZnSP03M6/screencap.png "screenshot")

### Build and install
#### Linux
requirements: git, Xlib\
\
installation:
- git clone https://gitlab.com/rmnsa/slstatus
- cd slstatus
- !! edit DEVICE entry in config.def.h to your network device name (to see all of them execute **ip link**) !!
- sudo make install clean
- !! add **slstatus &** to .xinitrc **before executing window manager** !!

Be sure to add this entry to .xinitrc before "exec dwm"\
slstatus &

### Configuration
configuration is done by editing config.h file and recompiling program\
\
\
\
!! this is not my project, its only slightly modified version of slstatus from [suckless.org](https://tools.suckless.org/slstatus) !!
